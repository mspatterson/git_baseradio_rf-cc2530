/***********************************************************************************
  Filename: light_switch.c

  Description:  This application function as a 802.15.4 Radio transmitter
  It is part of Tesco Base Radio Device.
  
   rev 0v09     Jan 21 2015     DD
    
***********************************************************************************/

/***********************************************************************************
* INCLUDES
*/
#include <hal_lcd.h>
#include <hal_led.h>
#include <hal_joystick.h>
#include <hal_assert.h>
#include <hal_board.h>
#include <hal_int.h>
#include "hal_mcu.h"
#include "hal_button.h"
#include "hal_rf.h"
#include "util_lcd.h"
#include "basic_rf.h"
#include "hal_timer_32k.h"
#include "hal_uart.h"
#include "CRC.h"

/***********************************************************************************
* CONSTANTS
*/
//Device Role - only one must be defined
// Do not change this
#define PROGRAM_COORDINATOR	1
	
// set the radio number
// 0 is the outside radio 
// 1 is the inside radio 
#define RADIO_NUMBER	0	 

// Application parameters
#if RADIO_NUMBER == 0
  #define RF_CHANNEL               11            // 2.4 GHz RF channel
#else
  #define RF_CHANNEL               25           // 2.4 GHz RF channel
#endif

// BasicRF address definitions
#define PAN_ID                0x2007
#define SWITCH_ADDR           0x2520
#define LIGHT_ADDR            0xBEEF
#define APP_PAYLOAD_LENGTH        128

// Application role
#define NONE                      0
#define SWITCH                    1
#define LIGHT                     2
#define APP_MODES                 2

#define ANTENNA_A					0
#define ANTENNA_B					1

#define IDLE                      0
#define POLL_UART_DATA	          0xAA

#define RF_TX_PERIOD			 32	// 32->0.97ms	// 313 -> 10ms
#define STATS_LENGTH			100
#define RF_HEADER_BYTES			10

/**********************************************************
Baud rate (bps) UxBAUD.BAUD_M UxGCR.BAUD_E Error(%)
2400 				59 		6 			0.14
9600 				59 		8 			0.14
57600 				216 		10 			0.03
115200 				216 		11 			0.03
230400 				216 		12 			0.03

Commonly used Baud Rate Settings for 32 MHz System Clock*/

// BAUD RATE 115,200
#define uartBaudM_115200		216
#define uartBaudE_115200		11

// BAUD RATE 9600
#define uartBaudM_9600		59
#define uartBaudE_9600		8

#define RX_DATA_SIZE	60

#define ENTER_PM_3	0XA7
#define POWER_MODE3	0x03

#define COMMAND		0X53
#define TX_DATA		0X29
#define RADIO_CHANGE_CHANNEL	0XA9
#define RADIO_CHANGE_POWER	0XA6
#define GET_RADIO_STATUS	0xB3

#define WTTTS_RESP_STREAM_DATA     0x24
#define WTTTS_RESP_REQ_FOR_CMD     0x26
#define WTTTS_RESP_VER_PKT         0x28
#define WTTTS_RESP_CFG_DATA        0x2A

#define RADIO_RX_ON_TIME	10      // time the Testork radio receiver will be on - in tens of milliseconds
/***********************************************************************************
* LOCAL VARIABLES
*/

// Define and allocate a setup structure for the UART protocol:
typedef struct {
	unsigned char uartNum : 1; // UART peripheral number (0 or 1)
	unsigned char START : 1; // Start bit level (low/high)
	unsigned char STOP : 1; // Stop bit level (low/high)
	unsigned char SPB : 1; // Stop bits (0 => 1, 1 => 2)
	unsigned char PARITY : 1; // Parity control (enable/disable)
	unsigned char BIT9 : 1; // 9 bit enable (8bit / 9bit)
	unsigned char D9 : 1; // 9th bit level or Parity type
	unsigned char FLOW : 1; // HW Flow Control (enable/disable)
	unsigned char ORDER : 1; // Data bit order(LSB/MSB first)
} UART_PROT_CONFIG;
UART_PROT_CONFIG __xdata uartProtConfig;

#define SIZE_OF_UART_RX_BUFFER 50
#define SIZE_OF_UART_TX_BUFFER SIZE_OF_UART_RX_BUFFER
// Allocate buffer+index for UART RX/TX
unsigned short __xdata uartRxBuffer[SIZE_OF_UART_RX_BUFFER];
unsigned short __xdata uartTxBuffer[SIZE_OF_UART_TX_BUFFER];
unsigned short __xdata uartRxIndex, uartTxIndex;

static uint8 pTxData[APP_PAYLOAD_LENGTH];
static uint8 pRxData[APP_PAYLOAD_LENGTH];
static basicRfCfg_t basicRfConfig;

//static uint32 dwCounterTx;

static uint8  bAntennaState;
static volatile uint8 appState;
static unsigned char TxBuffer[RX_DATA_SIZE];
static unsigned char RxBuffer[RX_DATA_SIZE];
static unsigned char pComData[RX_DATA_SIZE];
static uint8	RxBufIndex; 

static uint16 wUart1RcvdBytesOld, wUart1RcvdBytesNew =0;
static uint16 wUart0RcvdBytesOld, wUart0RcvdBytesNew =0;
//static uint16 wAppPayload;
static uint8 linkQualityIndicator;	
static uint8 pStatData[10];	
static uint8 txDataPending;
static uint16 wTxAppPayload;


#ifdef SECURITY_CCM
// Security key
static uint8 key[]= {
    0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7,
    0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf,
};
#endif

/***********************************************************************************
* LOCAL FUNCTIONS
*/
static void appLight();

static void appTimerISR(void);
static void appConfigTimer(uint16 cycles);
void appBuildPayload(uint8 *pTxDataBuf);
static void appUART0Init(void);
void uartInitProtocol(UART_PROT_CONFIG* uartProtConfig) ;
void uart0Send(unsigned char* uartTxBuf, unsigned short uartTxBufLength);


void Reset_MSP430F5419A_BSL(void);
void Reset_MSP430F5419A_Application(void);
void GetMSP430_BSL_Version(void);
static void appUART1Init(void);
void uart1Send(unsigned char* uartTxBuf, unsigned short uartTxBufLength);
uint16 GetMSP430_BSL_BufferSize(void);
void rRxBufGetAllData( char *buf);
void RxBufPut( char data);

void Uart1EnableRxInterrupt(void);
void RxBufReset(void);
void SetMSP430_BSL_Password(void);
void MSP430_BSL_Erase(void);
void MSP430_BSL_Get_Buf_Size(void);

void McuCommTask(void);
void HostCommTask(void);
void PowerDownMode(void);
void SendRadioStatus(void);

/***********************************************************************************
* @fn          appLight
*
* @brief       Application code for Base Radio transmitter. 
*              Transfer data and execute configuration commands.
*
* @param       none
*
* @return      none
*/
static void appLight()
{

	uint8 bPacketLength =0;

	// configure UART1 - USB Serial Interface 
	uartProtConfig.uartNum =  1;	// uart1
	uartProtConfig.START =	0;	// low start bit
	uartProtConfig.STOP = 1;	// high stop bit
	uartProtConfig.SPB	= 0;	// 1 stop bit
	uartProtConfig.PARITY = 0;	// parity disabled
	uartProtConfig.BIT9 = 0;	// 8 bit transfer
	uartProtConfig.D9 = 0;
	uartProtConfig.FLOW = 0;	// flow control disabled
	uartProtConfig.ORDER = 0;
	uartInitProtocol(&uartProtConfig) ;
	appUART1Init();
	
	// configure UART0 - Main MCU Serial Interface 
	halUartInit(HAL_UART_BAUDRATE_115966, 1);
	
	halTimer32kIntEnable();
	RxBufReset();
	Uart1EnableRxInterrupt();
		
    // Initialize BasicRF
    basicRfConfig.myAddr = LIGHT_ADDR;
    if(basicRfInit(&basicRfConfig)==FAILED) 
    {
      HAL_ASSERT(FALSE);
    }
    
    basicRfReceiveOn();

    while (TRUE) 
    {
        if(!basicRfPacketIsReady())
        {
            if(FSMSTAT0 ==0X11)
            {
                RFERRF = 0;	// ADDED DD
                RFIRQF0 = 0;	// ADDED DD
                RFIRQF1 = 0;	// ADDED DD
                basicRfReceiveOn();
            }
        }
        else
        {
            if((bPacketLength = basicRfReceive(pRxData, APP_PAYLOAD_LENGTH, NULL))>0) 
            {
                halLedSet(1);
                GetLinkQuality(&linkQualityIndicator);
                uart1Send((unsigned char*)pRxData, bPacketLength);
               
                halLedClear(1);
                
                if(txDataPending==TRUE)
                {
                    halLedSet(1);
                    if(basicRfSendPacket(SWITCH_ADDR, pTxData, wTxAppPayload)==FAILED)
                    {
                        if(basicRfSendPacket(SWITCH_ADDR, pTxData, wTxAppPayload)==FAILED)
                        {
                            
                        }
                    }
                    
                    txDataPending=FALSE;
                    halLedClear(1);
                    wUart1RcvdBytesOld = 0;
                    //RxBufReset();
                    //Uart1EnableRxInterrupt();
                    halUartEnableRxFlow(1); //signal that is ready to receive on UART
                    
                }
                
                
            }
        }
        
        
        if( appState == POLL_UART_DATA )
        {
            HostCommTask();
            McuCommTask();
            appState = IDLE;
        }
        
    }
}



/***********************************************************************************
* @fn          main
*
* @brief       This is the main entry of the "Light Switch" application.
*              After the application modes are chosen the switch can
*              send toggle commands to a light device.
*
* @param       basicRfConfig - file scope variable. Basic RF configuration
*              data
*              appState - file scope variable. Holds application state
*
* @return      none
*/
void main(void)
{
    appState = IDLE;
//	dwCounterTx = 0;
	
	linkQualityIndicator = 0;
        
        txDataPending = FALSE;
	
    // Config basicRF
    basicRfConfig.panId = PAN_ID;
    basicRfConfig.channel = RF_CHANNEL;
    basicRfConfig.ackRequest = TRUE;
#ifdef SECURITY_CCM
    basicRfConfig.securityKey = key;
#endif

		
// initialize UART 1 for a BSL communication with MSP430
// Baud rate is fixed to 9600 baud in half-duplex mode (one sender at a time).
// Start bit, 8 data bits (LSB first), an even parity bit, 1 stop bit.
// errata - The BSL does not expect a parity bit.	
	uartProtConfig.uartNum =  1;	// uart1
	uartProtConfig.START =	0;	// low start bit
	uartProtConfig.STOP = 1;	// high stop bit
	uartProtConfig.SPB	= 0;	// 1 stop bit
	uartProtConfig.PARITY = 0;	// parity disabled
	uartProtConfig.BIT9 = 0;	// 8 bit transfer
	uartProtConfig.D9 = 0;
	uartProtConfig.FLOW = 0;	// flow control disabled
	uartProtConfig.ORDER = 0;
    // Initalise board peripherals
    halBoardInit();
//    halJoystickInit();

    // Initalise hal_rf
    if(halRfInit()==FAILED) {
      HAL_ASSERT(FALSE);
    }

	
#ifdef PROGRAM_COORDINATOR	
	appConfigTimer(RF_TX_PERIOD);
	 appLight();
#endif			
    // Role is undefined. This code should not be reached
    HAL_ASSERT(FALSE);
}


static void appConfigTimer(uint16 cycles)
{
    halTimer32kInit(cycles);
    halTimer32kIntConnect(&appTimerISR);
}


/***********************************************************************************
* @fn          appTimerISR
*
* @brief       32KHz timer interrupt service routine. Signals PER test transmitter
*              application to transmit a packet by setting application state.
*
* @param       none
*
* @return      none
*/
static void appTimerISR(void)
{
    appState = POLL_UART_DATA;
}

/***********************************************************************************
* @fn          appUART0Init
*
* @brief       Initialize UART0
*              
*
* @param       none
*
* @return      none
*/
static void appUART0Init(void)
{
	// Configure UART0 for Alternative 1 => Port P0 (PERCFG.U0CFG = 0)
	PERCFG &= ~0x01;
	// Configure relevant Port P0 pins for peripheral function:
	// P0SEL.SELP0_2/3/4/5 = 1 => RX = P0_2, TX = P0_3, CT = P0_4, RT = P0_5
	P0SEL |= 0x3C;
	P0DIR |= BIT3;
	// Configure relevant Port P1 pins back to GPIO function
	P1SEL &= ~0x3C;
	
	// Initialize bitrate (U0BAUD.BAUD_M, U0GCR.BAUD_E)
	U0BAUD = uartBaudM_115200;
	U0GCR = (U0GCR&~0x1F) | uartBaudE_115200;
}


/***********************************************************************************
* @fn          uartInitProtocol
*
* @brief       This function initializes the UART protocol (start/stop bit, data bits,
*              parity, etc.). The application must call this function with an initialized
*              data structure according to the code in Figure 12.
*
* @param       pointer to UART configuration structure -UART_PROT_CONFIG
*
* @return      none
*/
void uartInitProtocol(UART_PROT_CONFIG* uartProtConfig) 
{
	// Initialize UART protocol for desired UART (0 or 1)
	if (uartProtConfig->uartNum == 0) {
		// USART mode = UART (U0CSR.MODE = 1)
		U0CSR |= 0x80;
		// Start bit level = low => Idle level = high (U0UCR.START = 0)
		// Start bit level = high => Idle level = low (U0UCR.START = 1)
		U0UCR = (U0UCR&~0x01) | uartProtConfig->START;
		// Stop bit level = high (U0UCR.STOP = 1)
		// Stop bit level = low (U0UCR.STOP = 0)
		U0UCR = (U0UCR&~0x02) | (uartProtConfig->STOP << 1);
		// Number of stop bits = 1 (U0UCR.SPB = 0)
		// Number of stop bits = 2 (U0UCR.SPB = 1)
		U0UCR = (U0UCR&~0x04) | (uartProtConfig->SPB << 2);
		// Parity = disabled (U0UCR.PARITY = 0)
		// Parity = enabled (U0UCR.PARITY = 1)
		U0UCR = (U0UCR&~0x08) | (uartProtConfig->PARITY << 3);
		// 9-bit data disable = 8 bits transfer (U0UCR.BIT9 = 0)
		// 9-bit data enable = 9 bits transfer (U0UCR.BIT9 = 1)
		U0UCR = (U0UCR&~0x10) | (uartProtConfig->BIT9 << 4);
		// Level of bit 9 = 0 (U0UCR.D9 = 0), used when U0UCR.BIT9 = 1
		// Level of bit 9 = 1 (U0UCR.D9 = 1), used when U0UCR.BIT9 = 1
		// Parity = Even (U0UCR.D9 = 0), used when U0UCR.PARITY = 1
		// Parity = Odd (U0UCR.D9 = 1), used when U0UCR.PARITY = 1
		U0UCR = (U0UCR&~0x20) | (uartProtConfig->D9 << 5);
		// Flow control = disabled (U0UCR.FLOW = 0)
		// Flow control = enabled (U0UCR.FLOW = 1)
		U0UCR = (U0UCR&~0x40) | (uartProtConfig->FLOW << 6);
		// Bit order = MSB first (U0GCR.ORDER = 1)
		// Bit order = LSB first (U0GCR.ORDER = 0) => For PC/Hyperterminal
		U0GCR = (U0GCR&~0x20) | (uartProtConfig->ORDER << 5);
	} else {
		// USART mode = UART (U1CSR.MODE = 1)
		U1CSR |= 0x80;
		// Start bit level = low => Idle level = high (U1UCR.START = 0)
		// Start bit level = high => Idle level = low (U1UCR.START = 1)
		U1UCR = (U1UCR&~0x01) | uartProtConfig->START;
		// Stop bit level = high (U1UCR.STOP = 1)
		// Stop bit level = low (U1UCR.STOP = 0)
		U1UCR = (U1UCR&~0x02) | (uartProtConfig->STOP << 1);
		// Number of stop bits = 1 (U1UCR.SPB = 0)
		// Number of stop bits = 2 (U1UCR.SPB = 1)
		U1UCR = (U1UCR&~0x04) | (uartProtConfig->SPB << 2);
		// Parity = disabled (U1UCR.PARITY = 0)
		// Parity = enabled (U1UCR.PARITY = 1)
		U1UCR = (U1UCR&~0x08) | (uartProtConfig->PARITY << 3);
		// 9-bit data enable = 8 bits transfer (U1UCR.BIT9 = 0)
		// 9-bit data enable = 8 bits transfer (U1UCR.BIT9 = 1)
		U1UCR = (U1UCR&~0x10) | (uartProtConfig->BIT9 << 4);
		// Level of bit 9 = 0 (U1UCR.D9 = 0), used when U1UCR.BIT9 = 1
		// Level of bit 9 = 1 (U1UCR.D9 = 1), used when U1UCR.BIT9 = 1
		// Parity = Even (U1UCR.D9 = 0), used when U1UCR.PARITY = 1
		// Parity = Odd (U1UCR.D9 = 1), used when U1UCR.PARITY = 1
		U1UCR = (U1UCR&~0x20) | (uartProtConfig->D9 << 5);
		// Flow control = disabled (U1UCR.FLOW = 0)
		// Flow control = enabled (U1UCR.FLOW = 1)
		U1UCR = (U1UCR&~0x40) | (uartProtConfig->FLOW << 6);
		// Bit order = MSB first (U1GCR.ORDER = 1)
		// Bit order = LSB first (U1GCR.ORDER = 0) => For PC/Hyperterminal
		U1GCR = (U1GCR&~0x20) | (uartProtConfig->ORDER << 5);
	}
}


/***********************************************************************************
* @fn          uart0Send
*
* @brief       Send Data on UART0 with carriage return and line feed
*              
*
* @param       pointer to data, data length
*
* @return      none
*/
void uart0Send(unsigned char* uartTxBuf, unsigned short uartTxBufLength)
{
	unsigned short uartTxIndex;
	UTX0IF = 0;
	for (uartTxIndex = 0; uartTxIndex < uartTxBufLength; uartTxIndex++) {
		U0DBUF = uartTxBuf[uartTxIndex];
		while( !UTX0IF );
		UTX0IF = 0;
	}
		U0DBUF = '\r';
		while( !UTX0IF );
		UTX0IF = 0;
		U0DBUF = '\n';
		while( !UTX0IF );
		UTX0IF = 0;

}

/***********************************************************************************
* @fn          appUART1Init
*
* @brief       Initialize UART1
*              
*
* @param       none
*
* @return      none
*/
static void appUART1Init(void)
{
	// Configure UART1 for Alternative 0 => Port P0 (PERCFG.U1CFG = 0)
	PERCFG &= ~0x02;
	// Configure relevant Port P0 pins for peripheral function:
	// P0SEL.SELP0_2/3/4/5 = 1 => RX = P0_5, TX = P0_4, CT = P0_2, RT = P0_3
	P0SEL |= BIT4|BIT5;
	P0DIR |= BIT4;
	P0DIR &= ~BIT5;
	// Configure relevant Port P1 pins back to GPIO function
	P1SEL &= ~0x3C;
	
	// Initialize bitrate (U0BAUD.BAUD_M, U0GCR.BAUD_E)
	U1BAUD = uartBaudM_115200;
	U1GCR = (U1GCR&~0x1F) | uartBaudE_115200;
}


/***********************************************************************************
* @fn          uart1Send
*
* @brief       Send Data on UART0 
*              
*
* @param       pointer to data, data length
*
* @return      none
*/
void uart1Send(unsigned char* uartTxBuf, unsigned short uartTxBufLength)
{
	unsigned short uartTxIndex;
	UTX1IF = 0;
	for (uartTxIndex = 0; uartTxIndex < uartTxBufLength; uartTxIndex++) {
		U1DBUF = uartTxBuf[uartTxIndex];
		while( !UTX1IF );
		UTX1IF = 0;
	}
	//at the end of the data packet must send "/r/n" in the shown order otherwise the software gives error.
	//this is only to use with LabView app
	// for other apps revise the ending if necessary!!!
//		U1DBUF = '\r';
//		while( !UTX1IF );
//		UTX1IF = 0;
//		U1DBUF = '\n';
//		while( !UTX1IF );
//		UTX1IF = 0;

}

/***********************************************************************************
* @fn          HAL_ISR_FUNCTION
*
* @brief       UART1 receive interrupt function
*              
*
* @param       none
*
* @return      none
*/
HAL_ISR_FUNCTION(uart1RxISR,URX1_VECTOR)
{
    uint8 c, s;

    URX1IF = 0;  
    c= U1DBUF;
    s= U1CSR;

   RxBufPut(c);
}

/***********************************************************************************
* @fn          RxBufPut
*
* @brief       add byte to the RX buffer
*              
*
* @param       byte
*
* @return      none
*/
void RxBufPut( char data)
{
	if (RxBufIndex < RX_DATA_SIZE)
		RxBuffer[RxBufIndex++] = data;
}

/***********************************************************************************
* @fn          rRxBufGetAllData
*
* @brief       read all data from the RX buffer. 
*              The calling function has to provide enough space in the receiving buffer.
*
* @param       pointer to destination array.
*
* @return      none
*/
void rRxBufGetAllData( char *buf)
{
	uint8 bIndex;
	
	for(bIndex = 0;bIndex<RxBufIndex; bIndex++)
	{
		*buf = RxBuffer[bIndex];
		buf++;
	}
		
	RxBufIndex = 0;

}

/***********************************************************************************
* @fn          Uart1EnableRxInterrupt
*
* @brief       Enable UART1 Rx interrupt
*              
*
* @param       none
*
* @return      none
*/
void Uart1EnableRxInterrupt(void)
{
	URX1IF = 0;       
	U1CSR |= 0x40;
	URX1IE = 1;
}


/***********************************************************************************
* @fn          RxBufReset
*
* @brief       Reset the RX buffer, by zeroing the index. 
*              For speed optimization this function does not clear the data.
*
* @param       none
*
* @return      none
*/
void RxBufReset(void)
{
	RxBufIndex = 0;
}


// HostCommTask
// updated march 04 2014

/***********************************************************************************
* @fn          HostCommTask
*
* @brief       Read data from UART1 (USB port) to a buffer. When a packet is received set a Tx pending flag. 
*              The packet is sent to the radio only after a packet is received from the radio. This way we reduce
*               the TesTORK radio ON time ,hence reducing the power
*
* @param       none
*
* @return      none
*/
void HostCommTask(void)
{
	
	// if there is new data on UART
            wUart1RcvdBytesNew = RxBufIndex;
            
            if (wUart1RcvdBytesOld>2)
            {
                    if(wUart1RcvdBytesOld == wUart1RcvdBytesNew)	
                    {
                            //wAppPayload = halUartRead(pTxData, wUartRcvdBytesNew);
                            wTxAppPayload  = RxBufIndex;
                            rRxBufGetAllData((char*) pTxData);
                            
                            if((pTxData[0]==0x46)||(pTxData[1]==0x46))
                            {
                                    halUartEnableRxFlow(0); //signal that is not ready to receive on UART
                                    //halLedSet(1);
                                    txDataPending = TRUE;
                            }
                            RxBufReset();
                            Uart1EnableRxInterrupt();
                            
                    }
                    else
                    {
                            wUart1RcvdBytesOld = wUart1RcvdBytesNew;
                    }
            }
            else
            {
                    wUart1RcvdBytesOld = wUart1RcvdBytesNew;
            }
}

/***********************************************************************************
* @fn          McuCommTask
*
* @brief       Read data from UART0 (MainMCU). The data are eihter commands or Over the air programming packet
*              Executes the command. Sends the OAP packets immediately.
*              
*
* @param       none
*
* @return      none
*/
void McuCommTask(void)
{
    uint16 wCnt;
    wUart0RcvdBytesNew = halUartGetNumRxBytes();
    
    if (wUart0RcvdBytesOld>=2)
    {
        if(wUart0RcvdBytesOld == wUart0RcvdBytesNew)	
        {
            wCnt = halUartRead(pComData, wUart0RcvdBytesNew);
            // the Command packets are max 3 bytes long
            // the OAP packets are at least 7 bytes long
            if((wCnt < 5)&&(pComData[0]==COMMAND))
            {
               
                switch(pComData[1])
                {
                  case ENTER_PM_3:
                    PowerDownMode();
                    break;
                  case RADIO_CHANGE_CHANNEL:
                    basicRfConfig.channel = pComData[2];  
                    //halRfSetChannel(basicRfConfig.channel);
                    halRfInit();
                    basicRfInit(&basicRfConfig);
                    basicRfReceiveOn();
                    break;
                  case RADIO_CHANGE_POWER:
                    TXPOWER = pComData[2];
                    break;
                  case GET_RADIO_STATUS:
                    SendRadioStatus();	
                    break;
                  default:
                    break;
                }
                
            }
            else
            {   // OAP packet  - send it as is.
                if(basicRfSendPacket(SWITCH_ADDR, pComData, wCnt)==FAILED)
                {
                    basicRfSendPacket(SWITCH_ADDR, pComData, wCnt);
                }
            }
        }
        else
        {
            wUart0RcvdBytesOld = wUart0RcvdBytesNew;
        }
    }
    else
    {
        wUart0RcvdBytesOld = wUart0RcvdBytesNew;
    }
    
}


/***********************************************************************************
* @fn          PowerDownMode
*
* @brief       Put the MCU to sleep
*              
*
* @param       none
*
* @return      none
*/
void PowerDownMode(void)
{
	halIntOff();
	SLEEPCMD |= POWER_MODE3;
	PCON = 0x01;
}
						  

/***********************************************************************************
* @fn          SendRadioStatus
*
* @brief       Send the Radio Status data
*              
*
* @param       none
*
* @return      none
*/						  
void SendRadioStatus(void)
{
  static uint16 sCounter;
	pStatData[0] = RADIO_NUMBER;
	pStatData[1] = basicRfConfig.channel;
	pStatData[2] = linkQualityIndicator;
	pStatData[3] = P0&BIT2;
	pStatData[4] = TXPOWER;
	halUartWrite(pStatData,5);
        sCounter++;
        
        if(sCounter>=500)
        {
          linkQualityIndicator = 0;
          sCounter = 0;
        }
}


/****************************************************************************************
  Copyright 2007 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
***********************************************************************************/
