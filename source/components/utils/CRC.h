#ifndef CRC_H_
#define CRC_H_

extern void crcReset(void);
extern void crcNextByte(unsigned char byte);
extern unsigned int crcGetValue(void);
extern unsigned char crcGetLowByte(void);
extern unsigned char crcGetHighByte(void);

#endif /*CRC_H_*/
